<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/home','SystemController@getInfo');
Route::get('/all','EnumberController@allEnumbers');
Route::get('/search/{data}','EnumberController@getEnumberByCrit');
Route::get('/enumber/type/{name}','EnumberController@Aggregator');
Route::get('/enumber/{id}','EnumberController@getInfo');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
