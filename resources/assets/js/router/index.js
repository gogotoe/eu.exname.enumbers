import Vue from 'vue'
import Router from 'vue-router'
import Table from '../components/Table.vue'
import Welcome from "../components/Welcome";
import Search from "../components/Search";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Welcome
        },
        {
            path: '/colours',
            name: 'Colours',
            component: Table
        },
        {
            path: '/preservatives',
            name: 'Preservatives',
            component: Table
        },
        {
            path: '/acids',
            name: 'Acids',
            component: Table
        },
        {
            path: '/stabilisers',
            name: 'Stabilisers',
            component: Table
        },
        {
            path: '/minerals',
            name: 'Minerals',
            component: Table
        },
        {
            path: '/flavors',
            name: 'Flavors',
            component: Table
        },
        {
            path: '/mixers',
            name: 'Mixers',
            component: Table
        },
        {
            path: '/dangerous',
            name: 'Dangerous',
            component: Table
        },
        {
            path: '/unsafe',
            name: 'Unsafe',
            component: Table
        },
        {
            path: '/harmless',
            name: 'Harmless',
            component: Table
        },
        {
            path: '/forbidden',
            name: 'Forbidden',
            component: Table
        },
        {
            path: '/allowed',
            name: 'Allowed',
            component: Table
        },
        {
            path: '/search',
            name: 'Search',
            component: Search
        }

    ]
})