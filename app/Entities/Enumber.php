<?php
/**
 * User: exname
 * Date: 2/23/19
 * Time: 1:31 PM
 */

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="enumber")
 */
class Enumber implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="integer")
     */
    private $forbidden;
    /**
     * @ORM\Column(type="integer")
     */
    private $toxic;

    /**
     * @ORM\OneToOne(targetEntity="Information")
     * @ORM\JoinColumn(name="infoId", referencedColumnName="id")
     */
    private $information;

    /**
     * @ORM\OneToMany(targetEntity="Link", mappedBy="e", cascade={"persist"})
     * @var ArrayCollection|Link[]
     */
    private $links;

    /**
     * Enumber constructor.
     * @param $id
     * @param $name
     * @param $forbidden
     * @param $toxic
     */
    public function __construct($id, $name, $forbidden, $toxic,$info)
    {
        $this->id = $id;
        $this->name = $name;
        $this->forbidden = $forbidden;
        $this->toxic = $toxic;
        $this->information=$info;
        $this->links = new ArrayCollection;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection|Link[]
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param Link $link
     * add a link for enumber
     */
    public function addLink(Link $link)
    {
        if (!$this->links->contains($link)) {
            $link->setEnumberId($this);
            $this->links->add($link);
        }

    }

    /**
     * @return mixed
     */
    public function getForbidden()
    {
        return $this->forbidden;
    }

    /**
     * @return mixed
     */
    public function getToxic()
    {
        return $this->toxic;
    }

    /**
     * @return mixed
     */
    public function getInformation()
    {
        return $this->information;
    }



    public function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "forbidden"=>$this->getForbidden(),
            "toxic"=>$this->getToxic()
        ];
    }

}