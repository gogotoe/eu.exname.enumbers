<?php
/**
 * User: exname
 * Date: 2/23/19
 * Time: 12:50 PM
 */

namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Links")
 */
class Link
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Enumber", inversedBy="links")
     * @var Enumber
     */
    private $e;

    /**
     * @ORM\Column(type="string")
     */
    private $link;

    /**
     * Links constructor.
     * @param $link
     */
    public function __construct($link)
    {
        $this->link = $link;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getE_id()
    {
        return $this->e;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $e_id
     */
    public function setE_id(Enumber $e_id)
    {
        $this->e = $e_id;
    }

    public function __toString()
    {
       return $this->link;
    }

}