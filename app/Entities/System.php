<?php
/**
 * User: exname
 * Date: 2/24/19
 * Time: 5:38 PM
 */

namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sys")
 */
class System
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $info;

    /**
     * System constructor.
     * @param $info
     */
    public function __construct($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

}