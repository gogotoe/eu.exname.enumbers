<?php

namespace App\Http\Controllers;

use App\Repository\SystemRepository as repo;

class SystemController extends Controller
{
    private $repo;
    public function __construct(repo $repo)
    {
        $this->repo = $repo;
    }
    public function getInfo(){
        return $this->repo->information();
    }
}
