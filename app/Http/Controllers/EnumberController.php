<?php

namespace App\Http\Controllers;

use App\Repository\EnumberRepository as repo;

class EnumberController extends Controller
{
    private $repo;

    public function __construct(repo $repo)
    {
        $this->repo = $repo;
    }

    public function edit($id = NULL)
    {
        return View('admin.index')->with(['data' => $this->repo->EnumberById($id)]);
    }

    public function editLink()
    {
    }

    public function getEnumber($id)
    {
        return $this->repo->EnumberById($id);
    }

    public function getEnumberByCrit($str)
    {
        if (is_numeric($str)) {
            return $this->repo->EnumberById($str);
        } else {
            return $this->repo->getEnumberByName($str);
        }
    }

    public function Aggregator($name)
    {
        switch ($name) {
            case "Colours":
                return $this->allEnumbersBetween(100, 199);
                break;
            case "Preservatives":
                return $this->allEnumbersBetween(200, 299);
                break;
            case "Antioxidants":
                return $this->allEnumbersBetween(300, 399);
                break;
            case "Stabilisers":
                return $this->allEnumbersBetween(400, 499);
                break;
            case "Minerals":
                return $this->allEnumbersBetween(500, 599);
                break;
            case "Flavors":
                return $this->allEnumbersBetween(600, 699);
                break;
            case "Mixers":
                return $this->allEnumbersBetween(900, 1520);
                break;//
            case "Dangerous":
                return $this->allEnumbersFindBy("toxic", 2);
                break;
            case "Unsafe":
                return $this->allEnumbersFindBy("toxic", 1);
                break;
            case "Harmless":
                return $this->allEnumbersFindBy("toxic", 0);
                break;
            case "Forbidden":
                return $this->allEnumbersFindBy("forbidden", 1);
                break;
            case "Allowed":
                return $this->allEnumbersFindBy("forbidden", 0);
                break;
        }
    }

    public function allEnumbersBetween($id1, $id2)
    {
        return $this->repo->getEnumbersBetween($id1, $id2);
    }

    public function allEnumbersFindBy($par, $val)
    {
        return $this->repo->getEnumbersFindBy($par, $val);
    }

    public function getInfo($id)
    {
        return $this->repo->EnumberInfoById($id);
    }

    public function allEnumbers()
    {
        return $this->repo->allEnumbers();
    }

}
