<?php

namespace App\Http\Controllers;
use App\Repository\InformationRepository as repo;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    private $repo;
    public function __construct(repo $repo)
    {
        $this->repo = $repo;
    }

    public function editInfo()
    {
        //
    }

    public function getLink($id){
        return $this->repo->GetInfo($id);
    }
}
