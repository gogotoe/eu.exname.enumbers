<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

namespace App\Http\Controllers;

use App\Repository\LinkRepository as repo;

class LinkController extends Controller
{
    private $repo;
    public function __construct(repo $repo)
    {
        $this->repo = $repo;
    }
    public function edit($id=NULL)
    {
        return View('admin.index')->with(['data' => $this->repo->LinkOfId($id)]);
    }
    public function editLink()
    {
    }
    public function getLink(){
        return $this->repo->LinkOfId(1);
    }
}
