<?php
/**
 * User: exname
 * Date: 2/23/19
 * Time: 5:26 PM
 */

namespace App\Repository;


use App\Entities\Enumber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\AST\LikeExpression;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EnumberRepository
{
    /**
     * @var string
     */
    private $class = 'App\Entities\Enumber';
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * LinkRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /*
     * CRUD
     * */
    public function create(Enumber $e)
    {
        $this->em->persist($e);
        $this->em->flush();
    }

    public function update(Enumber $e, $data)
    {
        if ($e == null) {
            throw new NotFoundHttpException();
        }
        $e->setE_id($data['e_id']);
        $this->em->persist($e);
        $this->em->flush();
    }

    public function delete(Enumber $e)
    {
        if ($e == null) {
            throw new NotFoundHttpException();
        }
        $this->em->remove($e);
        $this->em->flush();

    }

    public function EnumberById($id)
    {
        return $this->em->getRepository($this->class)->findOneBy(['id' => $id]);
    }
    public function EnumberInfoById($id)
    {
        return $this->EnumberById($id)->getInformation()->getInfo();

    }
    public function getEnumbersBetween($firstId,$lastId)
    {
        $qb=$this->em->getRepository($this->class)->createQueryBuilder('id');
        $data=$qb->where($qb->expr()->between('id',$firstId,$lastId))->getQuery()->getResult();
        return $data;

    }

    public function getEnumbersFindBy($par,$value)
    {
        return $this->em->getRepository($this->class)->findBy(array($par=>$value));

    }
    public function getEnumberByName($string){
        $qb= $this->em->getRepository($this->class)->createQueryBuilder('e');
        $data=$qb->where($qb->expr()->like('e.name',':str'))->setParameter('str','%'.$string.'%')->getQuery()->getResult();
        return $data;
    }
    public function allEnumbers()
    {
        return $this->em->getRepository($this->class)->findAll();
    }


}