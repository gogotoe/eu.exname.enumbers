<?php
/**
 * User: exname
 * Date: 2/24/19
 * Time: 5:40 PM
 */

namespace App\Repository;

use Doctrine\ORM\EntityManager;
class SystemRepository
{
    /**
     * @var string
     */
    private $class = 'App\Entities\System';
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * LinkRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /*
     * reading
     */
    public function information()
    {

        return $this->em->getRepository($this->class)->findOneBy(['id' => 1])->getInfo();//TODO: should be 'sign'=>id sign example is 304 or 205b
    }
}