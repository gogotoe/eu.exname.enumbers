<?php
/**
 * User: exname
 * Date: 2/23/19
 * Time: 5:26 PM
 */

namespace App\Repository;


use App\Entities\Information;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class InformationRepository
{
    /**
     * @var string
     */
    private $class = 'App\Entities\Information';
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * LinkRepository constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /*
     * CRUD
     * */
    public function create(Information $link)
    {
        $this->em->persist($link);
        $this->em->flush();
    }

    public function update(Information $link, $data)
    {
        if ($link == null) {
            throw new NotFoundHttpException();
        }
        $link->setE_id($data['e_id']);
        $this->em->persist($link);
        $this->em->flush();
    }

    public function delete(Information $link)
    {
        if ($link == null) {
            throw new NotFoundHttpException();
        }
        $this->em->remove($link);
        $this->em->flush();

    }

    public function GetInfo($id)
    {

        return $this->em->getRepository($this->class)->findOneBy(['id' => $id]);
    }


}